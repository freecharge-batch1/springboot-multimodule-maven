package com.freechage.customer.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/customer-service")
public class CustomerController {

    @GetMapping("/")
    public String sayHello(){
        return "Hello From Customer Service";
    }
}
