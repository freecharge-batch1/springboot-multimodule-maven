package com.freecharge.recharge.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/recharge-service")
public class RechargeController {
    @GetMapping("/")
    public String getHello(){
        return "hello from recharge-service";
    }
}
